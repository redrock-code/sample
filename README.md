# Installation

## Clone the repo

```
git clone https://redrock-code@bitbucket.org/redrock-code/sample.git
```
deploy with modman and (remove var/di directory if present) run:
```
php bin/magento module:enable RedRock_SampleChooser  
php bin/magento setup:upgrade
php bin/magento setup:static-content:deploy en_US
php bin/magento setup:di:compile
```
## Testing

create a few products and set the custom attribute "Is Sample" to yes; add a  product  
to cart and proceed to checkout, you will be redirected to a custom sample chooser of up to 3 samples.  
The sample count can be increased from the RedRock/SampleChooser/view/frontend/templates/samples/chooser.phtml  
component at the end of the template "maxSamples": N