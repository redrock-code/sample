<?php
/**
 * Copyright (c) 2016. RedRock
 * @author Marius Moldovan
 */
namespace RedRock\SampleChooser\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Response\RedirectInterface;

/**
 * select samples to be added
 *
 * Class Index
 * @package RedRock\SampleChooser\Controller\Index
 */
class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $cart;
    protected $redirect;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Cart $cart
     * @param RedirectInterface $redirectInterface
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Cart $cart,
        RedirectInterface $redirectInterface
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->cart = $cart;
        $this->redirect = $redirectInterface;
    }

    /**
     * @return bool|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        // only display samples chooser if there are products in cart
        if ($this->cart->getItemsCount() > 0) {
            return $this->resultPageFactory->create();
        }
        $this->redirect->redirect($this->getResponse(), 'checkout/cart');
        return false;
    }
}