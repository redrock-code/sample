<?php
/**
 * Copyright (c) 2016. RedRock
 * @author Marius Moldovan
 */
namespace RedRock\SampleChooser\Controller\Add;

use Magento\Framework\App\Action\Context;
use Magento\Catalog\Model\Product;
use Magento\Checkout\Model\Cart;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Response\RedirectInterface;

/**
 * add selected samples to cart
 *
 * Class Index
 * @package RedRock\SampleChooser\Controller\Add
 */
class Index extends \Magento\Framework\App\Action\Action
{
    protected $_cart;
    protected $_session;
    private $_redirectInterface;

    public function __construct(
        Context $context,
        Product $product,
        Session $session,
        RedirectInterface $redirectInterface,
        Cart $cart
    ) {
        parent::__construct($context);
        $this->_cart = $cart;
        $this->_session = $session;
        $this->_redirectInterface = $redirectInterface;
    }

    /**
     * add products to cart set session samples and redirect to checkout
     * contains sample count validation to max of 3 samples
     *
     * [TODO] add configurable sample count to admin config in future releases
     */
    public function execute()
    {

        $postData = $this->getRequest()->getParams();
        $productIds = explode(',', $postData['samples']);
        if (count($productIds) > 0) {
            $this->setSessionData('samples', $postData['samples']);
            $this->setSessionData('removed_samples', false);
            // slice the array of product ids to disable more than 3 samples added
            $productIds = array_slice($productIds, 0, 3);
            $this->_cart->addProductsByIds($productIds);
            $this->_cart->save();
        }
        $this->_redirectInterface->redirect($this->getResponse(), 'checkout');

    }

    /**
     * set session data
     *
     * @param $key
     * @param $value
     * @return mixed
     */
    public function setSessionData($key, $value)
    {
        return $this->_session->setData($key, $value);
    }
}