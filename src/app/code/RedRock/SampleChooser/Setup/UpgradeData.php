<?php
/**
 * Copyright (c) 2016. RedRock
 * @author Marius Moldovan
 */
namespace RedRock\SampleChooser\Setup;

use \Magento\Eav\Setup\EavSetup;
use \Magento\Eav\Setup\EavSetupFactory;
use \Magento\Framework\Setup\UpgradeDataInterface;
use \Magento\Framework\Setup\ModuleContextInterface;
use \Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if ( version_compare( $context->getVersion(), '2.1.0', '<' ) ) {
            /** @var EavSetup $eavSetup */
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            // add attribute to these product types only
            $productTypes = [
                \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE,
                \Magento\Catalog\Model\Product\Type::TYPE_VIRTUAL,
                \Magento\Downloadable\Model\Product\Type::TYPE_DOWNLOADABLE,
            ];
            $productTypes = join(',', $productTypes);
            /**
             * Add attributes to the eav/attribute
             */
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'is_sample',
                [
                    'type' => 'int',
                    'backend' => 'Magento\Catalog\Model\Product\Attribute\Backend\Boolean',
                    'frontend' => '',
                    'label' => 'Is Sample',
                    'input' => 'select',
                    'class' => '',
                    'source' => 'Magento\Catalog\Model\Product\Attribute\Source\Boolean',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => 0,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    // used for flat catalog product
                    'visible_on_front' => true,
                    // used for flat catalog product
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => $productTypes,
                    'sort' => 2,
                    'group' => 'General'

                ]
            );

        }
    }
}