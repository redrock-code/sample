<?php
/**
 * Copyright (c) 2016. RedRock
 * @author Marius Moldovan
 */
namespace RedRock\SampleChooser\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Cart;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Response\RedirectInterface;

/**
 * Class Checkout
 * @package RedRock\SampleChooser\Observer
 */
class Checkout implements ObserverInterface
{
    private $_context;
    private $_cart;
    private $_session;
    private $_redirectInterface;

    /**
     * Checkout constructor.
     *
     * @param Context $context
     * @param Cart $cart
     * @param Session $session
     * @param RedirectInterface $redirectInterface
     */
    public function __construct(
        Context $context,
        Cart $cart,
        Session $session,
        RedirectInterface $redirectInterface
    ) {
        $this->_context = $context;
        $this->_cart = $cart;
        $this->_session = $session;
        $this->_redirectInterface = $redirectInterface;
    }

    /**
     * redirect to samples chooser if no samples present
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->getSessionData('removed_samples') == true) {
            $this->_redirect($observer, 'sampleschooser');
        }
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @param $route
     */
    private function _redirect(\Magento\Framework\Event\Observer $observer, $route)
    {
        /** /Magento\Framework\App\Action\Action $controller */
        $controller = $observer->getControllerAction();
        $this->_redirectInterface->redirect($controller->getResponse(), $route);
    }

    /**
     * get session data
     *
     * @param $key
     * @param bool $remove
     * @return mixed
     */
    public function getSessionData($key, $remove = false)
    {
        return $this->_session->getData($key, $remove);
    }

}