<?php
/**
 * Copyright (c) 2016. RedRock
 * @author Marius Moldovan
 */
namespace RedRock\SampleChooser\Observer;

use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\App\Action\Context;
use \Magento\Customer\Model\Session;
use \RedRock\SampleChooser\Block\Samples;
use \Magento\Checkout\Model\Cart;

/**
 * Class Form
 * @package RedRock\SampleChooser\Observer
 */
class Cart implements ObserverInterface
{
    private $_context;
    private $_cart;
    private $_session;
    private $_samplesBlock;

    /**
     * Cart constructor.
     * @param Context $context
     * @param Cart $cart
     * @param Session $session
     * @param Samples $samples
     */
    public function __construct(
        Context $context,
        Cart $cart,
        Session $session,
        Samples $samples
    ) {
        $this->_context = $context;
        $this->_cart = $cart;
        $this->_session = $session;
        $this->_samplesBlock = $samples;
    }

    /**
     * remove all samples from cart
     * and set session removed samples flag
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->_removeAllSamples();
        $this->setSessionData('removed_samples', true);
    }

    /**
     * remove all samples from cart
     */
    private function _removeAllSamples()
    {
        $sampleCollection = $this->_samplesBlock->getSamples();
        foreach ($this->_cart->getItems() as $item) {
            foreach ($sampleCollection as $sampleItem) {
                if ($item->getProductId() == $sampleItem->getId()) {
                    $this->_cart->removeItem($item->getItemId());
                }
            }
        }
        $this->_cart->save();
    }

    /**
     * set session data
     *
     * @param $key
     * @param $value
     * @return mixed
     */
    public function setSessionData($key, $value)
    {
        return $this->_session->setData($key, $value);
    }

}