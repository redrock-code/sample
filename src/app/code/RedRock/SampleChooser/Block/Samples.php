<?php

/**
 * Copyright (c) 2016. RedRock
 * @author Marius Moldovan
 */
namespace RedRock\SampleChooser\Block;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\View\Element\Template\Context;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Helper\Image;
use Magento\Customer\Model\Session;

/**
 * Main Samples Block
 * @package RedRock\SampleChooser\Block
 */
class Samples extends \Magento\Framework\View\Element\Template
{
    private $_productCollectionFactory;
    private $_productStatus;
    private $_productVisibility;
    private $_productModel;
    private $_imageHelper;
    protected $_session;

    /**
     * Samples constructor.
     * @param Context $context
     * @param CollectionFactory $productCollectionFactory
     * @param Visibility $productVisibility
     * @param Status $productStatus
     * @param Product $productModel
     * @param Image $imageHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $productCollectionFactory,
        Visibility $productVisibility,
        Status $productStatus,
        Product $productModel,
        Image $imageHelper,
        Session $session,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productVisibility = $productVisibility;
        $this->_productStatus = $productStatus;
        $this->_productModel = $productModel;
        $this->_imageHelper = $imageHelper;
        $this->_session = $session;

    }

    /**
     * @return $this
     */
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    /**
     * get sample collection with custom attribute is_sample
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getSamples()
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->_productCollectionFactory->create()
            // only get enabled products
            ->addFieldToFilter('status',
                ['eq' => $this->_productStatus->getVisibleStatusIds()])
            ->addFieldToFilter('is_sample', true);
        return $collection;
    }

    /**
     * filter product collection based on previous choices saved in session
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getFilteredSamples()
    {
        $selected = $this->getSessionData('samples');
        $selected = count(explode(',', $selected)) > 0 ?
            explode(',', $selected) : array();
        $collection = $this->getSamples()
            ->addFieldToFilter('entity_id', ['nin' => $selected]);
        return $collection;
    }

    /**
     * get product from collection product interceptor
     *
     * @param int $id
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct($id)
    {
        $model = $this->_productModel;
        return $model->load($id);
    }

    /**
     * @return \Magento\Catalog\Helper\Image
     */
    public function getImageHelper()
    {
        return $this->_imageHelper;
    }

    /**
     * get session data
     *
     * @param $key
     * @param bool $remove
     * @return mixed
     */
    public function getSessionData($key, $remove = false)
    {
        return $this->_session->getData($key, $remove);
    }

}