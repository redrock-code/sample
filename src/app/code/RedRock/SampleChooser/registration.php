<?php
/**
 * Copyright (c) 2016. RedRock
 * @author Marius Moldovan
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'RedRock_SampleChooser',
    __DIR__
);
