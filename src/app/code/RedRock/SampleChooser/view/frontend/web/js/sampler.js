/*
 * Copyright (c) 2016. RedRock
 * @author Marius Moldovan
 */

/*jshint browser:true jquery:true expr:true*/
define([
    "jquery",
    "jquery/ui"
], function ($) {
    "use strict";

    $.widget('redrock.samplechooser', {
        options              : {},
        /**
         * widget creation
         *
         * @private
         */
        _create              : function () {
            // widget definition
            var sampler = this;
            // container
            var list = this.element.children('[data-role="samples"]');
            // sample collection
            var samples = this.element.find('[data-role="sample"]');
            this.options.inputSamples = $('#addSamples');
            // add sidebar object to options
            this.options.selected = this.element.children('[data-role="selected"]');
            this.options.list = list;
            this.options.form = this.element.find('[data-role="samples-form"]');
            samples.each(
                function () {
                    var sample = $(this);
                    sample.on('click', $.proxy(
                        function (event) {
                            var sampleId = sample.data('id');
                            sampler._action(sample, sampleId)
                        }, this)
                    )
                }
            );

            this.element.find('[data-role="submit"]').each(
                function () {
                    var submit = $(this);
                    submit.on('click', $.proxy(
                        function (event) {
                            var form = sampler.options.form;
                            console.log(form);
                            form.submit();
                        }, this)
                    );
                }
            );
            // update disable / enabled flag for samples
            this._updateEnabledSamples();
        },
        /**
         * call appropriate action based on context
         *
         * @param sample
         * @param sampleId
         * @private
         */
        _action              : function (sample, sampleId) {
            this._validateSamples();
            if (sample.parent().data('role') == 'samples' && this.options.maxCondition) {
                this._addSample(sample, sampleId);
            }
            else if (sample.parent().data('role') == 'selected') {
                this._removeSample(sample, sampleId);
            }
            this._updateEnabledSamples();
        },
        /**
         * add sample
         *
         * @private
         */
        _addSample           : function (sample, sampleId) {
            var selected = this.options.selected;
            selected.append(sample);
            this._addValue(sampleId);
        },
        /**
         * remove sample
         *
         * @private
         */
        _removeSample        : function (sample, sampleId) {
            var list = this.options.list;
            list.append(sample);
            this._removeValue(sampleId);
        },
        /**
         * disables or enables the list
         *
         * @private
         */
        _updateEnabledSamples: function () {
            this._validateSamples();

            if (!this.options.maxCondition) {
                this.options.list.attr('data-disabled', 'true');
            }
            else {
                this.options.list.attr('data-disabled', 'false');
            }
        },
        /**
         * validate samples added
         *
         * @private
         */
        _validateSamples     : function () {
            this.options.maxCondition =
                this.options.maxSamples > this.options.selected.children().length;
        },
        /**
         * add id to input
         *
         * @param value
         * @private
         */
        _addValue            : function (value) {
            var input = this.options.inputSamples;
            var ids = input.val();
            var sampleIds = ids.length > 0 ? ids.split(',') : [];
            sampleIds.push(value);
            input.val(sampleIds.join());
        },
        /**
         * remove id from input
         *
         * @param value
         * @private
         */
        _removeValue         : function (value) {
            var input = this.options.inputSamples;
            var ids = input.val();
            var sampleIds = ids.split(',');
            for (var i = 0; i < sampleIds.length; i++) {
                if (parseInt(value) == parseInt(sampleIds[i]) ||
                    '' == sampleIds[i]
                ) {
                    sampleIds.splice(i, 1);
                }
            }
            input.val(sampleIds.join());

        }
    });
    return $.redrock.samplechooser;
});