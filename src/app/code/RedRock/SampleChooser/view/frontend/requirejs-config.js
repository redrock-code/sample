/*
 * Copyright (c) 2016. RedRock
 * @author Marius Moldovan
 */

var config = {
    "map": {
        "*" : {
            sampler : 'RedRock_SampleChooser/js/sampler'
        }
    }
};